class Person:
    def __init__(self, name, job="Unemployed", pay=0.):
        self.__name = name
        self.__job = job
        self.__pay = pay

    def get_name(self):
        return self.__name

    def get_job(self):
        return self.__job

    def set_job(self, job):
        self.__job = job

    def get_pay(self):
        return self.__pay

    def get_last_name(self):
        return self.__name.split()[-1]

    def give_raise(self, percent):
        self.__pay = self.__pay * self.__get_raise_multiplier(percent)

    def __str__(self):
        return "[Person: " + self.__name + ", Job: " + self.__job + ", Pay: " + str(self.__pay) + "]"

    def __get_raise_multiplier(self, percent):
        return 1 + percent
