import person

if __name__ == "__main__":
    bob = person.Person("Bob Smith")
    sue = person.Person("Sue Jones", "Developer", 10000)
    print(bob)
    print(sue)
    print(bob.get_last_name(), sue.get_last_name())
    sue.give_raise(.10)
    print(sue)
    bob.set_job("Engineer")
    print(bob)
